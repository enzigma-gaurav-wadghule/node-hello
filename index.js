var express = require('express');
var app = express();

app.get('/', function(req, res){
  res.send(`Hi welcome ${req.query.name} Current time is ${Date()}`);
});

app.get('/form', function(req, res){
    res.write(`<!DOCTYPE html>
    <html>
    <body>
    <h2>HTML Forms</h2>
    <form action="http://localhost:3000/">
      <label for="fname">First name:</label><br>
      <input type="text" id="fname" name="name" value="John"><br>
      <label for="lname">Last name:</label><br>
      <input type="text" id="lname" name="lname" value="Doe"><br><br>
      <input type="submit" value="Submit">
    </form> 
    </body>
    </html>
    `);
});

app.listen(3000);